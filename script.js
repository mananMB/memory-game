const GRID_FOR = {
  landscape: {
    2: {
      columns: 2,
      rows: 2,
    },
    4: {
      columns: 4,
      rows: 2,
    },
    8: {
      columns: 4,
      rows: 4,
    },
    12: {
      columns: 6,
      rows: 4,
    },
  },
  portrait: {
    2: {
      columns: 2,
      rows: 2,
    },
    4: {
      columns: 2,
      rows: 4,
    },
    8: {
      columns: 4,
      rows: 4,
    },
    12: {
      columns: 4,
      rows: 6,
    },
  },
};

const gameContainer = document.getElementById("game");
const playAgain = document.getElementById("play-again");
const levelButtons = document.getElementById("level-buttons");
const winCard = document.getElementById("win-card");
const levelSelector = document.getElementById("level-selector");
const startButton = document.getElementById("start-button");
const startMenu = document.getElementById("start-menu");
const quit = document.getElementById("quit");
const endLevel = document.getElementById("end-level");
const scoreboard = document.getElementById("scoreboard");
const score = document.getElementById("score");
const highScore = document.getElementById("high-score");
const scoreBoardButtons = document.getElementById(
  "scoreboard-buttons-container"
);
const restartLevel = document.getElementById("restart-level");
const overlay = document.getElementById("overlay");
const howToPlayModalButton = document.getElementById("how-to-play-button");
const howToPlayModal = document.getElementById("how-to-play-modal");
const closeHowToPlayModal = document.getElementById("close-how-to-play-modal");
const endLevelModal = document.getElementById("end-level-modal");
const endLevelModalYesButton = document.getElementById("end-level-modal-yes");
const endLevelModalNoButton = document.getElementById("end-level-modal-no");
const restartLevelModal = document.getElementById("restart-level-modal");
const restartLevelModalYesButton = document.getElementById(
  "restart-level-modal-yes"
);
const restartLevelModalNoButton = document.getElementById(
  "restart-level-modal-no"
);
const winCardScore = document.getElementById("win-card-score");
const portraitMessage = document.getElementById("portrait-message-modal");
const mainGameContainer = document.getElementById("game-container");
const disable = document.getElementById("disable");

let cardsSelected = [];
let matchesFound = 0;
let gameSize = 0;
let flipCounter = 0;
let gameStarted = false;

const selectGifs = () => {
  let gifList = Array(12)
    .fill(0)
    .map((id, index) => index + 1);

  gifList = shuffle(gifList);
  gifList.length = gameSize;

  return shuffle([...gifList, ...gifList]);
};

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

const setGridPortrait = () => {
  gameContainer.style.gridTemplateColumns = `repeat(${GRID_FOR.portrait[gameSize].columns},1fr)`;
  gameContainer.style.gridTemplateRows = `repeat(${GRID_FOR.portrait[gameSize].rows},1fr)`;
};

const setGridLandscape = () => {
  gameContainer.style.gridTemplateColumns = `repeat(${GRID_FOR.landscape[gameSize].columns},1fr)`;
  gameContainer.style.gridTemplateRows = `repeat(${GRID_FOR.landscape[gameSize].rows},1fr)`;
};

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function generateCards(gifIDs) {
  matchesFound = 0;
  cardsSelected = [];
  flipCounter = 0;
  score.innerText = "Flips: 0";
  if (!localStorage[`highScore${gameSize}`]) {
    localStorage[`highScore${gameSize}`] = "undefined";
  }
  if (localStorage[`highScore${gameSize}`] !== "undefined") {
    highScore.innerText = `Best: ${localStorage[`highScore${gameSize}`]}`;
  } else {
    highScore.innerText = "";
  }
  winCard.style.display = "none";
  gameContainer.style.display = "grid";
  if (window.innerWidth < window.innerHeight) {
    setGridPortrait();
  } else {
    setGridLandscape();
  }
  gameContainer.innerHTML = "";
  
  const cards = gifIDs.map((id) => {
    const card = document.createElement("div");
    card.classList.add("card");
    card.classList.add(`gif${id}`);

    const front = document.createElement("div");
    front.className = "front-card";

    const back = document.createElement("div");
    back.className = "back-card";

    const image = document.createElement("img");
    image.style.height = "100%";
    image.style.width = "auto";
    image.src = `gifs/${id}.gif`;
    image.alt = "gif";

    back.appendChild(image);

    card.appendChild(front);
    card.appendChild(back);

    // call a function handleCardClick when a div is clicked on
    card.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    return card;
  });
  
  gameContainer.append(...cards);
  gameStarted = true;
  setTimeout(() => {
    mainGameContainer.scrollIntoView({ behavior: "smooth" });
  }, 500);
}

const unselectCards = (cardsSelected) => {
  setTimeout(() => {
    cardsSelected[0].classList.remove("selected");
    cardsSelected[1].classList.remove("selected");
    disable.style.display = "none";
  }, 1000);
};

const flipCard = (event) => {
  if (cardsSelected.length < 2) {
    cardsSelected.push(event.target.parentElement);
    event.target.parentElement.classList.add("selected");
    score.innerText = `Flips: ${++flipCounter}`;
  }

  if (cardsSelected.length === 2) {
    disable.style.display = "block";
    // console.log(cardsSelected[0], cardsSelected[1])
    // There can be two selected cards at most at once.
    // classList[1] will always have the id of the gif.
    if (cardsSelected[0].classList[1] === cardsSelected[1].classList[1]) {
      matchesFound++;
      cardsSelected[0].classList.add("foundMatch");
      cardsSelected[1].classList.add("foundMatch");
      disable.style.display = "none";
    } else {
      unselectCards(cardsSelected);
    }
    cardsSelected = [];

    if (matchesFound === gameSize) {
      gameStarted = false;
      if (
        localStorage[`highScore${gameSize}`] === "undefined" ||
        localStorage[`highScore${gameSize}`] > flipCounter
      ) {
        console.log("updating highscore");
        localStorage[`highScore${gameSize}`] = flipCounter;
      }
      setTimeout(() => {
        winCardScore.innerText = `Flips: ${flipCounter}`;
        gameContainer.style.display = "none";
        winCard.style.display = "flex";
        playAgain.style.display = "block";
        scoreboard.style.display = "none";
      }, 1200);
    }
  }
};

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  event.stopPropagation();

  if (
    event.target.className === "front-card" &&
    !event.target.parentElement.className.includes("selected") &&
    !event.target.parentElement.className.includes("foundMatch")
  ) {
    flipCard(event);
  }
}

// when the DOM loads

levelButtons.addEventListener("click", (event) => {
  if (event.target.className.includes("level-select")) {
    gameSize = Number(event.target.dataset.gameBoardSize);
    generateCards(selectGifs());
    scoreboard.style.display = "flex";
    levelSelector.style.display = "none";
  }
});

playAgain.onclick = () => {
  levelSelector.style.display = "flex";
  winCard.style.display = "none";
  gameContainer.style.display = "none";
  playAgain.style.display = "none";
};

startButton.onclick = () => {
  startMenu.style.display = "none";
  levelSelector.style.display = "flex";
};

quit.onclick = () => {
  startMenu.style.display = "flex";
  levelSelector.style.display = "none";
};

endLevel.onclick = () => {
  endLevelModal.style.display = "flex";
  overlay.style.display = "block";
};

endLevelModalYesButton.onclick = () => {
  endLevelModal.style.display = "none";
  overlay.style.display = "none";
  gameContainer.style.display = "none";
  scoreboard.style.display = "none";
  levelSelector.style.display = "flex";
};

endLevelModalNoButton.onclick = () => {
  endLevelModal.style.display = "none";
  overlay.style.display = "none";
};

restartLevel.onclick = () => {
  restartLevelModal.style.display = "flex";
  overlay.style.display = "block";
};

restartLevelModalYesButton.onclick = () => {
  restartLevelModal.style.display = "none";
  overlay.style.display = "none";
  generateCards(selectGifs());
};

restartLevelModalNoButton.onclick = () => {
  restartLevelModal.style.display = "none";
  overlay.style.display = "none";
};

howToPlayModalButton.onclick = () => {
  overlay.style.display = "block";
  howToPlayModal.style.display = "flex";
};

closeHowToPlayModal.onclick = () => {
  howToPlayModal.style.display = "none";
  overlay.style.display = "none";
};

window.addEventListener("resize", () => {
  if (gameStarted) {
    if (window.innerWidth < window.innerHeight) {
      setGridPortrait();
    } else {
      setGridLandscape();
    }
  }
});

const showLandscapeModeMessageOnMobile = () => {
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    if (window.innerWidth > window.innerHeight) {
      endLevelModal.style.display = "none";
      restartLevelModal.style.display = "none";
      howToPlayModal.style.display = "none";
      portraitMessage.style.display = "flex";
      overlay.style.display = "block";
      window.scroll({ top: 0, behavior: "smooth" });
    } else {
      portraitMessage.style.display = "none";
      overlay.style.display = "none";
    }
  }
};

showLandscapeModeMessageOnMobile();

window.addEventListener("orientationchange", () => {
  setTimeout(() => {
    showLandscapeModeMessageOnMobile();
  }, 1000);
});
